package task2

import (
	"testing"

	"gitlab.com/serboox/golang-autocode/src/task2"
)

func equal(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Fatalf("Not equal: \n"+
			"expected: %d\n"+
			"actual  : %d", expected, actual)
	}
}

func TestCalculate(t *testing.T) {
	as := task2.ArithmeticSequence{}
	equal(t, -1, as.Calculate(2, 3, -2))
	equal(t, 0, as.Calculate(2, 3, 0))
	equal(t, 2, as.Calculate(2, 3, 1))
	equal(t, 7, as.Calculate(2, 3, 2))
	equal(t, 15, as.Calculate(2, 3, 3))
	equal(t, 26, as.Calculate(2, 3, 4))
	equal(t, 40, as.Calculate(2, 3, 5))
}
