package task2

// ArithmeticSequence empty struct
type ArithmeticSequence struct{}

// Calculate method for calculating the arithmetic sequence
/*func (as *ArithmeticSequence) Calculate(number, add, count int) int {
	// put your code here
	panic("NotImplementedException")
	return
}*/

// Calculate method for calculating the arithmetic sequence
func (as *ArithmeticSequence) Calculate(number, add, count int) int {
	if count < 0 {
		return -1
	}

	switch count {
	case 0:
		return 0
	case 1:
		return number
	}

	res, buff := number, number

	for i := 2; i <= count; i++ {
		buff += add
		res += buff
	}

	return res
}
