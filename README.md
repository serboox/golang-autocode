# Golang for Autocode
> https://autocode.lab.epam.com/

## Task Description

> Implement method *Calculate(a, r, n)*, should return the sum of the first *n*
elements of a sequence in which each element is the sum of the given integer *a*,
and a number of occurences of the given integer *r*, based on the element's 
position within the sequence.

For example: 
```
Calculate(2, 3, 5) -> 40
```
```
0   1     2        3          4            5
0 + a + (a+r) + (a+r+r) + (a+r+r+r) + (a+r+r+r+r) 
0 + 2 + (2+3) + (2+3+3) + (2+3+3+3) + (2+3+3+3+3) = 40

To DO:
Fork this project to your gitLab
Implement your code
Build solution
Push your changes
Submit your solution using AutoCode application
See your Build and tests results
Note, that you may Submit your solution not more than 3 times for every task
Good luck and Happy coding!
